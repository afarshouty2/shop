/**
 * 
 */
package com.fouad.eshop.enums;

/**
 * @author Ahmed
 * 
 * An Enum to hold supported time ranges 
 */
public enum TimeRange {

	TODAY("Today"),
	LAST_MONTH("lastMonth");
	
	private String value;
	
	private TimeRange(String value){
		this.value = value;
	}

	/**
	 * @return the value
	 */
	public String getValue() {
		return value;
	}

	/**
	 * @param value the value to set
	 */
	public void setValue(String value) {
		this.value = value;
	}
	
	/**
	 * Loads the enum member equivalent to a string value
	 * 
	 * @param value
	 * @return
	 */
	public static TimeRange fromString(String value) {
	    for (TimeRange tP : TimeRange.values()) {
	      if (tP.value.equalsIgnoreCase(value)) {
	        return tP;
	      }
	    }
	    return null;
	  }
	
}
