/**
 * 
 */
package com.fouad.eshop.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fouad.eshop.dto.StatisticsDto;
import com.fouad.eshop.enums.TimeRange;
import com.fouad.eshop.service.StatisticsService;

/**
 * @author Ahmed
 * 
 *         Controller class for Statistics related services
 *
 */
@RestController
public class StatisticsController {

	@Autowired
	private StatisticsService statisticsService;

	@RequestMapping(value = "statistics", method = RequestMethod.GET)
	public ResponseEntity<StatisticsDto> genearateStatistics(
			@RequestParam(value = "time", required = true) String timePeriod) throws CloneNotSupportedException {
		return new ResponseEntity<>(statisticsService.getStatisticsForTimePeriod(TimeRange.fromString(timePeriod)),
				HttpStatus.OK);
	}

}
