/**
 * 
 */
package com.fouad.eshop.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fouad.eshop.dto.ProductStockDto;
import com.fouad.eshop.exception.NoStockFoundException;
import com.fouad.eshop.exception.OutDatedStockUpdateException;
import com.fouad.eshop.exception.ProductIdNotFoundException;
import com.fouad.eshop.exception.StockIdNotRelatedToProductIdException;
import com.fouad.eshop.request.StockUpdateRequest;
import com.fouad.eshop.service.ProductStockService;

/**
 * @author Ahmed
 * 
 *         Controller class for Product Stock related services
 *
 */
@RestController
public class ProductStockController {

	@Autowired
	private ProductStockService productStockService;

	@RequestMapping(value = "stock", method = RequestMethod.GET)
	public ResponseEntity<ProductStockDto> getProductStockById(
			@RequestParam(value = "productId", required = true) String productId)
			throws ProductIdNotFoundException, NoStockFoundException {
		return new ResponseEntity<>(productStockService.getProductStock(productId), HttpStatus.OK);
	}

	@PostMapping(value = "stock")
	public ResponseEntity<HttpStatus> updateStock(@Valid @RequestBody StockUpdateRequest stockInfo)
			throws ProductIdNotFoundException, OutDatedStockUpdateException, StockIdNotRelatedToProductIdException {
		productStockService.updateStock(stockInfo);
		return new ResponseEntity<>(HttpStatus.OK);
	}

}
