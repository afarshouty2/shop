/**
 * 
 */
package com.fouad.eshop.controller.error.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import com.fouad.eshop.dto.ErrorDto;
import com.fouad.eshop.exception.NoStockFoundException;
import com.fouad.eshop.exception.OutDatedStockUpdateException;
import com.fouad.eshop.exception.ProductIdNotFoundException;
import com.fouad.eshop.exception.StockIdNotRelatedToProductIdException;

/**
 * @author Ahmed
 *
 * An aggregated error handling class 
 */
@ControllerAdvice
public class ErrorHandler {
	
	private final Logger logger = LoggerFactory.getLogger(this.getClass());
	
	@ExceptionHandler(Throwable.class)
	public ResponseEntity<ErrorDto> handleThrowable(Throwable ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("An error occurred while perfroming the operation!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("Unhandled Exception Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(Exception.class)
	public ResponseEntity<ErrorDto> handleGeneralException(Exception ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("An error occurred while perfroming the operation!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("General Exception Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.INTERNAL_SERVER_ERROR);
	}
	
	@ExceptionHandler(OutDatedStockUpdateException.class)
	public ResponseEntity<ErrorDto> handleOutDatedUpdateException(OutDatedStockUpdateException ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("Unacceptable stock update error!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("OutDatedStockUpdateException Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.NO_CONTENT);
	}
	
	@ExceptionHandler(ProductIdNotFoundException.class)
	public ResponseEntity<ErrorDto> handleProductIdNotFoundException(ProductIdNotFoundException ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("Input productId is not found in DB!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("ProductIdNotFoundException Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.NO_CONTENT);
	}
	
	@ExceptionHandler(NoStockFoundException.class)
	public ResponseEntity<ErrorDto> handleNoStockFoundException(NoStockFoundException ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("Input productId has no stock in DB!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("NoStockFoundException Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.NO_CONTENT);
	}
	
	
	@ExceptionHandler(StockIdNotRelatedToProductIdException.class)
	public ResponseEntity<ErrorDto> handleStockIdNotRelatedToProductIdException(StockIdNotRelatedToProductIdException ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("Stock, and Product Mismatch!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("StockIdNotRelatedToProductIdException Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
	
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public ResponseEntity<ErrorDto> handleInvalidInputParameterException(MethodArgumentNotValidException ex){
		ErrorDto error = new ErrorDto();
		error.setMessage("Input validattion Error!");
		error.setExceptionMessage(ex.getMessage());
		logger.debug("MethodArgumentNotValidException Occurred! ", ex);
		return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
	}
}
