/**
 * 
 */
package com.fouad.eshop.entity;

import java.time.Instant;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

import com.fouad.eshop.enums.TimeRange;

/**
 * @author Ahmed
 * 
 * A utility class for time related functionalities
 */
public class TimeUtils {

	private TimeUtils(){}
	
	public static String getCurrentUTCTimeInString(){
		Instant instant = Instant.now();
		return instant.toString();
	}
	
	public static String getCurrentUTCTimeInString(Date timeStamp){
		Instant instant = Instant.ofEpochMilli(timeStamp.getTime());
		return instant.toString();
	}
	
	public static Date getCurrentUTCTime(){
		return Calendar.getInstance(TimeZone.getTimeZone("UTC")).getTime();
	}
	
	public static Date getStartTimeFromRange(TimeRange range){
		Calendar calendar = Calendar.getInstance(TimeZone.getTimeZone("UTC"));
		calendar.set(Calendar.HOUR_OF_DAY, 0);
		calendar.set(Calendar.MINUTE, 0);
		calendar.set(Calendar.SECOND, 0);
	    Date date = calendar.getTime();
	    
	    if(range.equals(TimeRange.TODAY)){
			return date;
		}
	    
	    if(range.equals(TimeRange.LAST_MONTH)){
	    	calendar.set(Calendar.DAY_OF_MONTH, calendar.get(Calendar.DAY_OF_MONTH) - 30);
	    	date = calendar.getTime();
	    	return date;
	    }
	    
		return null;
	}
}
