/**
 * 
 */
package com.fouad.eshop.entity;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * @author Ahmed
 * 
 *         Entity Class for "stock" Table
 *
 */

@Entity
@Table(name = "stock")
@NamedQueries({ @NamedQuery(name = "Stock.findAll", query = "SELECT s FROM Stock s") })
public class Stock implements Serializable {

	private static final long serialVersionUID = 3099626584090412367L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	private Integer id;
	
	@Column(name = "stock_id")
	private String stockId;
	
	@Column(name = "available_quantity")
	private Integer availableQuantity;
	
	@Column(name = "items_sold_quantity")
	private Integer itemsSoldQuantity;
	
	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "product_ref_id")
	private Product product;

	@Column(name = "last_updated_datetime")
	@Temporal(TemporalType.TIMESTAMP)
	private Date lastUpdatedDateTime;
	
	
	
	public Stock(){
		lastUpdatedDateTime = TimeUtils.getCurrentUTCTime();
		itemsSoldQuantity = 0;
	}
	
	public Stock(Integer id, String stockId, Integer availableQuantity, Integer itemsSoldQuantity){
		this();
		this.id = id;
		this.stockId = stockId;
		this.availableQuantity = availableQuantity;
		this.itemsSoldQuantity = itemsSoldQuantity;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}

	/**
	 * @return the availableQuantity
	 */
	public Integer getAvailableQuantity() {
		return availableQuantity;
	}

	/**
	 * @param availableQuantity the availableQuantity to set
	 */
	public void setAvailableQuantity(Integer availableQuantity) {
		this.availableQuantity = availableQuantity;
	}

	/**
	 * @return the itemsSoldQuantity
	 */
	public Integer getItemsSoldQuantity() {
		return itemsSoldQuantity;
	}

	/**
	 * @param itemsSoldQuantity the itemsSoldQuantity to set
	 */
	public void setItemsSoldQuantity(Integer itemsSoldQuantity) {
		this.itemsSoldQuantity = itemsSoldQuantity;
	}

	/**
	 * @return the product
	 */
	public Product getProduct() {
		return product;
	}

	/**
	 * @param product the product to set
	 */
	public void setProduct(Product product) {
		this.product = product;
	}

	/**
	 * @return the lastUpdatedDateTime
	 */
	public Date getLastUpdatedDateTime() {
		return lastUpdatedDateTime;
	}

	/**
	 * @param lastUpdatedDateTime the lastUpdatedDateTime to set
	 */
	public void setLastUpdatedDateTime(Date lastUpdatedDateTime) {
		this.lastUpdatedDateTime = lastUpdatedDateTime;
	}

	/**
	 * @return the serialversionuid
	 */
	public static long getSerialversionuid() {
		return serialVersionUID;
	}

	/**
	 * @return the stockId
	 */
	public String getStockId() {
		return stockId;
	}

	/**
	 * @param stockId the stockId to set
	 */
	public void setStockId(String stockId) {
		this.stockId = stockId;
	}

}
