/**
 * 
 */
package com.fouad.eshop.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fouad.eshop.dao.StockRepository;
import com.fouad.eshop.dto.StatisticsDto;
import com.fouad.eshop.entity.Stock;
import com.fouad.eshop.entity.TimeUtils;
import com.fouad.eshop.enums.TimeRange;
import com.fouad.eshop.mapper.StatisticsMapper;

/**
 * @author Ahmed
 *
 *         A service class for Statistics generation related business logic
 */
@Service
public class StatisticsService {

	@Autowired
	private StockRepository stockRepository;

	/**
	 * Generates Stock statistics for specific time range
	 * 
	 * @param range
	 * @return
	 * @throws CloneNotSupportedException
	 */
	public StatisticsDto getStatisticsForTimePeriod(TimeRange range) throws CloneNotSupportedException {
		StatisticsDto statisticsDto = new StatisticsDto(range);
		List<Stock> topAvaialableStockEntries = stockRepository
				.findTop3ByLastUpdatedDateTimeBetweenOrderByAvailableQuantityDesc(
						TimeUtils.getStartTimeFromRange(range), TimeUtils.getCurrentUTCTime());
		List<Stock> topSellingProducts = stockRepository
				.findTop3ByLastUpdatedDateTimeBetweenOrderByItemsSoldQuantityDesc(
						TimeUtils.getStartTimeFromRange(range), TimeUtils.getCurrentUTCTime());
		statisticsDto.setTopAvailableProducts(
				StatisticsMapper.mapStockListToTopAvailableProdctsList(topAvaialableStockEntries));
		statisticsDto.setTopSellingProducts(StatisticsMapper.mapStockListTopSellingProductsList(topSellingProducts));
		return statisticsDto;
	}

}
