/**
 * 
 */
package com.fouad.eshop.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Isolation;
import org.springframework.transaction.annotation.Transactional;

import com.fouad.eshop.dao.ProductRepository;
import com.fouad.eshop.dao.StockRepository;
import com.fouad.eshop.dto.ProductStockDto;
import com.fouad.eshop.entity.Product;
import com.fouad.eshop.entity.Stock;
import com.fouad.eshop.exception.NoStockFoundException;
import com.fouad.eshop.exception.OutDatedStockUpdateException;
import com.fouad.eshop.exception.ProductIdNotFoundException;
import com.fouad.eshop.exception.StockIdNotRelatedToProductIdException;
import com.fouad.eshop.mapper.ProductStockMapper;
import com.fouad.eshop.request.StockUpdateRequest;

/**
 * @author Ahmed
 * 
 *         A service class for product stock related business logic
 *
 */

@Service
public class ProductStockService {

	@Autowired
	private ProductRepository productRepository;

	@Autowired
	private StockRepository stockRepository;

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	public ProductStockDto getProductStock(String productId) throws ProductIdNotFoundException, NoStockFoundException {
		Product product = productRepository.findBySkuId(productId);
		if (product == null || product.getId() == null) {
			throw new ProductIdNotFoundException(productId);
		} else if (product.getStock() == null || product.getStock().getId() == null) {
			throw new NoStockFoundException(productId);
		}
		return ProductStockMapper.mapProdudctToDto(product);

	}

	@Transactional(isolation = Isolation.REPEATABLE_READ)
	public void updateStock(StockUpdateRequest stockUpdateRequest)
			throws ProductIdNotFoundException, OutDatedStockUpdateException, StockIdNotRelatedToProductIdException {
		Stock stock = stockRepository.findByStockId(stockUpdateRequest.getId());

		if (stock == null || stock.getId() == null) {
			stock = prepareStockForInsert(stockUpdateRequest);
		} else {

			if (!stock.getProduct().getSkuId().equals(stockUpdateRequest.getProductId())) {
				throw new StockIdNotRelatedToProductIdException(stockUpdateRequest.getId(),
						stockUpdateRequest.getProductId());
			}

			if (stock.getLastUpdatedDateTime().before(stockUpdateRequest.getTimeStamp())) {
				prepareStockForUpdate(stockUpdateRequest, stock);
			} else {
				throw new OutDatedStockUpdateException();
			}
		}
		stockRepository.save(stock);
	}

	private Stock prepareStockForInsert(StockUpdateRequest stockUpdateRequest) throws ProductIdNotFoundException {
		Stock newStock = new Stock();
		newStock.setStockId(stockUpdateRequest.getId());
		Product product = productRepository.findBySkuId(stockUpdateRequest.getProductId());
		if (product == null || product.getId() == null) {
			throw new ProductIdNotFoundException(stockUpdateRequest.getProductId());
		}
		newStock.setProduct(product);
		return newStock;
	}

	private void prepareStockForUpdate(StockUpdateRequest stockUpdateRequest, Stock stock) {
		if ((stock.getAvailableQuantity() - stockUpdateRequest.getQuantity()) > 0) {
			Integer newItemsSoldQuantity = (stock.getAvailableQuantity() - stockUpdateRequest.getQuantity())
					+ stock.getItemsSoldQuantity();
			stock.setItemsSoldQuantity(newItemsSoldQuantity);
		}
		stock.setAvailableQuantity(stockUpdateRequest.getQuantity());
		stock.setLastUpdatedDateTime(stockUpdateRequest.getTimeStamp());
	}

}
