/**
 * 
 */
package com.fouad.eshop.exception;

/**
 * @author Ahmed
 * 
 * A custom exception for missing the product id in DB
 */
public class OutDatedStockUpdateException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public OutDatedStockUpdateException(){
	}

    public OutDatedStockUpdateException(String message, Throwable cause) {
        super(message, cause);
    }

    public OutDatedStockUpdateException(Throwable cause) {
        super(cause);
    }
	
	
}
