/**
 * 
 */
package com.fouad.eshop.exception;

/**
 * @author Ahmed
 * 
 * A custom exception for missing the product id in DB
 */
public class ProductIdNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public ProductIdNotFoundException(String productId){
		super("Product Id: "+ productId +" Not Found");
	}

    public ProductIdNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProductIdNotFoundException(Throwable cause) {
        super(cause);
    }
	
	
}
