/**
 * 
 */
package com.fouad.eshop.exception;

/**
 * @author Ahmed
 * 
 * A custom exception for missing stock info for a specific product Id
 */
public class NoStockFoundException extends Exception {

	private static final long serialVersionUID = 1L;
	
	public NoStockFoundException(String productId){
		super("No stock found for prductId : "+productId);
	}

    public NoStockFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NoStockFoundException(Throwable cause) {
        super(cause);
    }
	
	
}
