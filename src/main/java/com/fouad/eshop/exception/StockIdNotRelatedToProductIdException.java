/**
 * 
 */
package com.fouad.eshop.exception;

/**
 * @author Ahmed
 * 
 * A custom exception for stock id mismatching product id
 */
public class StockIdNotRelatedToProductIdException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	public StockIdNotRelatedToProductIdException(String stockId, String productId){
		super("Stock Id : " + stockId+" is not related to Product Id: "+ productId);
	}

    public StockIdNotRelatedToProductIdException(String message, Throwable cause) {
        super(message, cause);
    }

    public StockIdNotRelatedToProductIdException(Throwable cause) {
        super(cause);
    }
	
	
}
