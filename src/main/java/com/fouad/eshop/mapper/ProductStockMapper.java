/**
 * 
 */
package com.fouad.eshop.mapper;

import com.fouad.eshop.dto.ProductStockDto;
import com.fouad.eshop.dto.StockDto;
import com.fouad.eshop.entity.Product;
import com.fouad.eshop.entity.Stock;

/**
 * @author Ahmed
 * 
 * A utility class to map product entity to DTO  
 *
 */
public class ProductStockMapper {
	
	private ProductStockMapper(){
		
	}

	/**
	 * Maps the Product entity to the corresponding DTO
	 * 
	 * @param product
	 * @return
	 */
	public static ProductStockDto mapProdudctToDto(Product product){
		ProductStockDto productStockDto = new ProductStockDto();
		productStockDto.setProductId(product.getSkuId());
		productStockDto.setStock(mapStockToDto(product.getStock()));
		return productStockDto;
	}
	
	/**
	 * Maps the Stock entity to the corresponding DTO
	 * 
	 * @param stock
	 * @return
	 */
	private static StockDto mapStockToDto(Stock stock){
		StockDto stockDto = new StockDto();
		stockDto.setId(stock.getStockId());
		stockDto.setQuantity(stock.getAvailableQuantity());
		stockDto.setTimeStamp(stock.getLastUpdatedDateTime());
		return stockDto;
	}
	
}
