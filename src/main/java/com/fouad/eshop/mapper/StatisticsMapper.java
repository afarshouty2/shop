/**
 * 
 */
package com.fouad.eshop.mapper;

import java.util.ArrayList;
import java.util.List;

import com.fouad.eshop.dto.TopAvailableProduct;
import com.fouad.eshop.dto.TopSellingProduct;
import com.fouad.eshop.entity.Stock;
import com.fouad.eshop.entity.TimeUtils;

/**
 * @author Ahmed
 *
 *         A mapping class to statistics
 */
public class StatisticsMapper {
	
	private StatisticsMapper() {
	}

	public static List<TopAvailableProduct> mapStockListToTopAvailableProdctsList(List<Stock> stocks) throws CloneNotSupportedException{
		List<TopAvailableProduct> topAvailableProducts = new ArrayList<>();
		TopAvailableProduct topAvailableProduct;
		for(Stock stock : stocks){
			topAvailableProduct = new TopAvailableProduct();
			topAvailableProduct.setId(stock.getStockId());
			topAvailableProduct.setProductId(stock.getProduct().getSkuId());
			topAvailableProduct.setQuantity(stock.getAvailableQuantity());
			topAvailableProduct.setTimestamp(TimeUtils.getCurrentUTCTimeInString(stock.getLastUpdatedDateTime()));
			topAvailableProducts.add((TopAvailableProduct)topAvailableProduct.clone());
		}
		return topAvailableProducts;
	}
	
	
	public static List<TopSellingProduct> mapStockListTopSellingProductsList(List<Stock> stocks) throws CloneNotSupportedException{
		List<TopSellingProduct> topSellingProducts = new ArrayList<>();
		TopSellingProduct topSellingProduct;
		for(Stock stock : stocks){
			topSellingProduct = new TopSellingProduct();
			topSellingProduct.setProductId(stock.getProduct().getSkuId());
			topSellingProduct.setItemsSold(stock.getItemsSoldQuantity());
			topSellingProducts.add((TopSellingProduct)topSellingProduct.clone());
		}
		return topSellingProducts;
	}
	
}
