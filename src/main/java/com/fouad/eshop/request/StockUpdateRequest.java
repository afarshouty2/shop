/**
 * 
 */
package com.fouad.eshop.request;

import java.util.Date;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author Ahmed
 * 
 * A class holding request body for Stock update API
 *
 */
public class StockUpdateRequest {
	
	@NotBlank
	private String id;
	@NotBlank
	private String productId;
	@NotNull
	private Date timeStamp;
	@NotNull
	private Integer quantity;
	
	/**
	 * 
	 * @param id
	 * @param productId
	 * @param timeStamp
	 * @param quantity
	 */
	public StockUpdateRequest(String id, String productId, Date timeStamp, Integer quantity){
		this.id = id;
		this.productId = productId;
		this.timeStamp = timeStamp;
		this.quantity = quantity;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the timeStamp
	 */
	public Date getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = timeStamp;
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
}
