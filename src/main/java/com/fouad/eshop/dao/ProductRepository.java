/**
 * 
 */
package com.fouad.eshop.dao;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fouad.eshop.entity.Product;

/**
 * @author Ahmed
 *
 * A CRUD Repository for Product entity
 */
@Repository
public interface ProductRepository extends CrudRepository<Product, Long> {
	Product findBySkuId(@Param("skuId") String skuId);
}
