/**
 * 
 */
package com.fouad.eshop.dao;

import java.util.Date;
import java.util.List;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.fouad.eshop.entity.Stock;

/**
 * @author Ahmed
 *
 * A CRUD Repository for Stock entity
 */
@Repository
public interface StockRepository extends CrudRepository<Stock, Long> {
	
	Stock findByStockId(@Param("stockId") String stockId);
	List<Stock> findTop3ByLastUpdatedDateTimeBetweenOrderByAvailableQuantityDesc(Date start, Date end);
	List<Stock> findTop3ByLastUpdatedDateTimeBetweenOrderByItemsSoldQuantityDesc(Date start, Date end);
}
