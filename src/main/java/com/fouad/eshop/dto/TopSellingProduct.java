
package com.fouad.eshop.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "productId",
    "itemsSold"
})
public class TopSellingProduct implements Cloneable {

    @JsonProperty("productId")
    private String productId;
    @JsonProperty("itemsSold")
    private Integer itemsSold;

    @JsonProperty("productId")
    public String getProductId() {
        return productId;
    }

    @JsonProperty("productId")
    public void setProductId(String productId) {
        this.productId = productId;
    }

    @JsonProperty("itemsSold")
    public Integer getItemsSold() {
        return itemsSold;
    }

    @JsonProperty("itemsSold")
    public void setItemsSold(Integer itemsSold) {
        this.itemsSold = itemsSold;
    }

    public Object clone() throws CloneNotSupportedException{
    	return super.clone();
    }
}
