/**
 * 
 */
package com.fouad.eshop.dto;

/**
 * @author Ahmed
 *
 * A class holding error info for exception handling
 */
public class ErrorDto {

	private String message; // A friendly representation of the error
	private String exceptionMessage; //The thrown exception message 
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	/**
	 * @return the exceptionMessage
	 */
	public String getExceptionMessage() {
		return exceptionMessage;
	}
	/**
	 * @param exceptionMessage the exceptionMessage to set
	 */
	public void setExceptionMessage(String exceptionMessage) {
		this.exceptionMessage = exceptionMessage;
	}
	
	
	
}
