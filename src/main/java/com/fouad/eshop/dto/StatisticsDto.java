
package com.fouad.eshop.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;
import com.fouad.eshop.entity.TimeUtils;
import com.fouad.eshop.enums.TimeRange;

@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "requestTimestamp",
    "range",
    "topAvailableProducts",
    "topSellingProducts"
})
public class StatisticsDto {

    @JsonProperty("requestTimestamp")
    private String requestTimestamp;
    @JsonProperty("range")
    private String range;
    @JsonProperty("topAvailableProducts")
    private List<TopAvailableProduct> topAvailableProducts = null;
    @JsonProperty("topSellingProducts")
    private List<TopSellingProduct> topSellingProducts = null;

    public StatisticsDto() {
		this.requestTimestamp = TimeUtils.getCurrentUTCTimeInString();
	}
    
    public StatisticsDto(TimeRange range) {
    	this();
		this.range = range.getValue();
	}
    
    
    @JsonProperty("requestTimestamp")
    public String getRequestTimestamp() {
        return requestTimestamp;
    }

    @JsonProperty("requestTimestamp")
    public void setRequestTimestamp(String requestTimestamp) {
        this.requestTimestamp = requestTimestamp;
    }

    @JsonProperty("range")
    public String getRange() {
        return range;
    }

    @JsonProperty("range")
    public void setRange(String range) {
        this.range = range;
    }

    @JsonProperty("topAvailableProducts")
    public List<TopAvailableProduct> getTopAvailableProducts() {
        return topAvailableProducts;
    }

    @JsonProperty("topAvailableProducts")
    public void setTopAvailableProducts(List<TopAvailableProduct> topAvailableProducts) {
        this.topAvailableProducts = topAvailableProducts;
    }

    @JsonProperty("topSellingProducts")
    public List<TopSellingProduct> getTopSellingProducts() {
        return topSellingProducts;
    }

    @JsonProperty("topSellingProducts")
    public void setTopSellingProducts(List<TopSellingProduct> topSellingProducts) {
        this.topSellingProducts = topSellingProducts;
    }

}
