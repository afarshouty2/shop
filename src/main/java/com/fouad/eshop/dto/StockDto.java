/**
 * 
 */
package com.fouad.eshop.dto;

import java.util.Date;

import com.fouad.eshop.entity.TimeUtils;

/**
 * @author Ahmed
 * 
 * A Data Transfer Object holding Sotck info
 *
 */
public class StockDto {
	
	private String id;
	private String timeStamp;
	private Integer quantity;
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}
	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(Date timeStamp) {
		this.timeStamp = TimeUtils.getCurrentUTCTimeInString(timeStamp);
	}
	/**
	 * @return the quantity
	 */
	public Integer getQuantity() {
		return quantity;
	}
	/**
	 * @param quantity the quantity to set
	 */
	public void setQuantity(Integer quantity) {
		this.quantity = quantity;
	}
	
	

}
