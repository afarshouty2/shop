/**
 * 
 */
package com.fouad.eshop.dto;

import com.fouad.eshop.entity.TimeUtils;

/**
 * @author Ahmed
 * 
 *         A Data transfer object holding information about product stock
 *
 */
public class ProductStockDto {

	private String productId;
	private String requestTimestamp;
	private StockDto stock;

	public ProductStockDto() {
		this.requestTimestamp = TimeUtils.getCurrentUTCTimeInString();
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId
	 *            the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the requestTimestamp
	 */
	public String getRequestTimestamp() {
		return requestTimestamp;
	}

	/**
	 * @return the stock
	 */
	public StockDto getStock() {
		return stock;
	}

	/**
	 * @param stock
	 *            the stock to set
	 */
	public void setStock(StockDto stock) {
		this.stock = stock;
	}

}
