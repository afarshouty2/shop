/**
 * 
 */
package com.fouad.eshop.dao.test;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.fouad.eshop.dao.ProductRepository;
import com.fouad.eshop.entity.Product;
import com.fouad.eshop.entity.Stock;

/**
 * @author Ahmed
 *
 *         A Unit Testing class covering the product stock related service
 */

@RunWith(SpringRunner.class)
@DataJpaTest
public class ProductRepositoryIntegerationTest {

	@Autowired
	private ProductRepository productRepository;

	@Test
	public void whenFindBySkuId_thenReturnProduct() {
		Product iPhoneProduct = new Product();
		iPhoneProduct.setId(1);
		iPhoneProduct.setSkuId("iphone-10");
		iPhoneProduct.setName("iPhone X");
		iPhoneProduct.setStock(new Stock(1, "000001", 10, 0));

		productRepository.save(iPhoneProduct);

		Product product = productRepository.findBySkuId("iphone-10");
		assertThat(product.getName()).isEqualTo(iPhoneProduct.getName());

		assertThat(product.getStock().getStockId()).isEqualTo(iPhoneProduct.getStock().getStockId());

	}

}
