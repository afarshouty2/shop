/**
 * 
 */
package com.fouad.eshop.service.test;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Calendar;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;

import com.fouad.eshop.dao.ProductRepository;
import com.fouad.eshop.dao.StockRepository;
import com.fouad.eshop.dto.ProductStockDto;
import com.fouad.eshop.entity.Product;
import com.fouad.eshop.entity.Stock;
import com.fouad.eshop.entity.TimeUtils;
import com.fouad.eshop.exception.NoStockFoundException;
import com.fouad.eshop.exception.OutDatedStockUpdateException;
import com.fouad.eshop.exception.ProductIdNotFoundException;
import com.fouad.eshop.exception.StockIdNotRelatedToProductIdException;
import com.fouad.eshop.request.StockUpdateRequest;
import com.fouad.eshop.service.ProductStockService;

/**
 * @author Ahmed
 *
 *         A unit testing class for ProductStockService test
 */
@RunWith(SpringRunner.class)
public class ProductStockServiceTest {

	@TestConfiguration
	static class ProductServiceTestContextConfiguration {

		@Bean
		public ProductStockService productStockService() {
			return new ProductStockService();
		}
	}

	@Autowired
	private ProductStockService productStockService;

	@MockBean
	private ProductRepository productRepository;

	@MockBean
	private StockRepository stockRepository;

	private final String PRODUCT_ID = "iphone-10";
	private final String STOCK_ID = "000001";

	@Before
	public void setUp() {
		Product iPhoneProduct = new Product();
		Stock iPhoneStock = new Stock(1, STOCK_ID, 10, 0);

		iPhoneProduct.setId(1);
		iPhoneProduct.setSkuId(PRODUCT_ID);
		iPhoneProduct.setName("iPhone X");
		iPhoneProduct.setStock(iPhoneStock);

		Mockito.when(productRepository.findBySkuId(iPhoneProduct.getSkuId())).thenReturn(iPhoneProduct);

		Mockito.when(stockRepository.save(ArgumentMatchers.<Stock>any())).thenReturn(iPhoneStock);
	}

	@Test
	public void testProductStockRetrieval() throws ProductIdNotFoundException, NoStockFoundException {
		ProductStockDto productStockDto = productStockService.getProductStock(PRODUCT_ID);

		assertThat(productStockDto.getProductId()).isEqualTo(PRODUCT_ID);
	}

	@Test
	public void testProductStockUpdate()
			throws OutDatedStockUpdateException, StockIdNotRelatedToProductIdException, ProductIdNotFoundException,
			NoStockFoundException {
		StockUpdateRequest stockUpdateRequest = new StockUpdateRequest(STOCK_ID, PRODUCT_ID,
				TimeUtils.getCurrentUTCTime(), 20);

		productStockService.updateStock(stockUpdateRequest);
		
	}

}
