# Shop Stock

In an online shop we would like to provide for all available products the stock data. Additionally
for analytic reasons, we also want to keep track on some basic statistics.

## Getting Started

This application is created to avail three RESTful endpoints:

## **GET /stock?productId=<productId>**
To retireve the porduct info including stock details.

### Example:

#### Request URL:

localhost:8080/stock?productId=samsung-note-9

#### Sample Response:
```javascript
{
    "productId": "samsung-note-9",
    "requestTimestamp": "2018-09-29T22:45:33.362Z",
    "stock": {
        "id": "000002",
        "timeStamp": "2018-09-29T15:08:57.595Z",
        "quantity": 20
    }
}
```
#### Response Codes:

* HTTP Status Code 200 - For Success
* HTTP Status Code 204 - For Outdated Stock Update
* HTTP Status Code 500 - For general internal excpetions
* HTTP Status Code 400 - For invalid input parapmeters

## **POST /updateStock**
To update the stock for a specific product.

### Consumes: 

application/json

### Example:

#### Request URL:
localhost:8080/stock

#### Reqest Body:

```javascript
{
	"id":"000003",
	"timeStamp":"2018-09-29T22:45:33.362Z",
	"productId":"hauawei-mate-10",
	"quantity":0
}
```

#### Response Codes: 

* HTTP Status Code 200 - For Success
* HTTP Status Code 204 - For Outdated Stock Update
* HTTP Status Code 500 - For general internal excpetions
* HTTP Status Code 400 - For invalid input parapmeters

## **GET /statistics?time=[today,lastMonth]**
Generateing statistics for the top 3 available products, alongside with the top 3 sold products
- The available ranges are:
1- today: returning the top products from midnight until now.
2- lastMonth: returning the top products from thirty days in the past till now.

### Example:

#### Request URL:

localhost:8080/statistics?time=lastMonth

#### Sample Response:

```javascript
{
    "requestTimestamp": "2018-09-30T08:48:37.187Z",
    "range": "lastMonth",
    "topAvailableProducts": [
        {
            "id": "000005",
            "timestamp": "2018-09-28T00:00:00Z",
            "productId": "samsung-note-8",
            "quantity": 50
        },
        {
            "id": "000004",
            "timestamp": "2018-09-28T00:00:00Z",
            "productId": "hauawei-p-20",
            "quantity": 40
        },
        {
            "id": "000002",
            "timestamp": "2018-09-29T22:45:33.362Z",
            "productId": "samsung-note-9",
            "quantity": 21
        }
    ],
    "topSellingProducts": [
        {
            "productId": "hauawei-mate-10",
            "itemsSold": 33
        },
        {
            "productId": "samsung-note-8",
            "itemsSold": 5
        },
        {
            "productId": "hauawei-p-20",
            "itemsSold": 4
        }
    ]
}
```
#### Response Codes:

* HTTP Status Code 200 - For Success
* HTTP Status Code 500 - For general unhandled excpetions

### Prerequisites

Having Docker installed

### Setup


```shell
docker pull afarshouty7188/shop-stock:0.0.2-SNAPSHOT
```
```shell
docker run -d -p 8080:8080 afarshouty7188/shop-stock:0.0.2-SNAPSHOT
```

## Authors

* **Ahmed Fouad** - *Initial work*